"""
Fabric Deployment Script

For more help on a command, run fab -d <command>

Role definitions correspond with deployment environments. You will want to
specify one or more roles when running a command, eg:

    fab -R staging deploy
    fab -R staging,production deploy

See the ``show_env`` command for help on available environment variables.

-------------------------------------------------------------------------------
"""
import re
from functools import wraps

import paramiko
from datetime import datetime
from fabric.api import (cd, env, sudo, puts, execute, require, prompt,
                        hide, abort, settings, local, shell_env)
from fabric.api import run as fab_run
from fabric.decorators import task as fab_task
from fabric.colors import cyan, red

# --| Deployment Configuration |-----------------------------------------------
env.forward_agent = True

# Role Definitions
# ----------------
# Each role represents a deployment environment and may contain multiple hosts.
# In this fabfile, one host must not belong to multiple roles.
env.roledefs.update({
    'production': [
        'web1.skinnyit.com',
    ],
    # 'staging': [
    #     'sta1.skinnyit.com',
    # ],
})

# Default Fabric environment variables
# -----------------------------
# Each value is formatted using value.format(**env), so order is important
# here. These values are overridden by role_defaults and anything passed in via
# --set. Built-in fabric environment variables always take precidence over
# these defaults.

defaults = (
    ('user', 'deploy'),
    ('base_dir', '/var/www/skinnyit'),
    ('owner', '{user}'),
    ('git_user', '{user}'),
    ('default_branch', 'master'),
    ('venv', '{base_dir}/env'),
    ('src_dir', '{base_dir}/src'),
    ('python', '{venv}/bin/python'),
    ('requirements', '{src_dir}/skinnyit/requirements/{envname}.txt'),
    ('manage_script', '{base_dir}/manage.py'),
    ('git_remote', 'origin'),
    ('touch_reload', '{base_dir}/reload'),
)

# Role-specific defaults
# ----------------------
# Same as above, but specific to the given role. These override above defaults.

role_defaults = {
    'production': (
        ('envname', 'production'),
    ),
    'staging': (
        ('envname', 'staging'),
    )
}


# --| Task definitions |------------------------------------------------------

# Extend the task decorator to support our defaults and role-specific defaults
def task(func):
    """
    Extends Fabric's @task decorator to support the environment defaults.

    Each value is formatted using value.format(**env), so order is important
    here. These values are overridden by role_defaults and anything passed in
    via --set. Built-in fabric environment variables always take precidence
    over these defaults.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        # setup envrronment
        roles = [r for r, h in env.roledefs.items() if env.host_string in h]
        if len(roles) > 1:
            msg = "Host {0} exists in multiple roles: {1}"
            abort(msg.format(env.host_string, ', '.join(roles)))

        role = None
        if len(roles) == 1:
            role = roles[0]

        # update defaults with role-based defaults
        env_defaults = defaults + role_defaults.get(role, ())

        # update environment.
        deferred = None
        while deferred is None or len(deferred) > 0:
            deferred = []
            for k, v in env_defaults:
                try:
                    env[k] = v.format(**env)
                except KeyError:
                    deferred.append((k, v))
        func(*args, **kwargs)
    return fab_task(wrapper)


@task
def manage(cmd, **kwargs):
    """
    Runs a django management command on the server.

    Usage:

        manage:"<command>"
        manage:"<command>",FOO=foo,BAR=bar

    ``command`` is the command and arguments you would normally pass to
    Django's ``manage.py`` script. It's best to always put the command in
    quotes to avoid it being misinterpreted.

    The remaining keyword arguments are passed as shell variables when the
    management command is executed.

    This command runs as the ``owner`` environment variable.
    """
    require('python', 'manage_script', 'owner', 'src_dir')
    manage = "{python} {manage_script} ".format(**env)
    with cd(env.src_dir):
        if kwargs:
            with shell_env(**kwargs):
                run(manage + cmd, user=env.owner)
        else:
            run(manage + cmd, user=env.owner)


@task
def set_branch(branch=None):
    """
    Switches the branch of the deployed git clone.

    Usage:

        set_branch
        set_branch:<branchname>

    If ``branchname`` is not provided, your current local branch will be used,
    or "master" if you are on a detached HEAD.

    If branch has not been created in the remote clone, it will be created
    automatically by calling the following git command:

        git checkout -b <branchname> <git_remote>/<branchname>

    By default, the ``git_remote`` environment variable is set to "origin".
    You can override the ``git_remote`` environment variable using --set.
    """
    require('git_remote')
    if branch is None:
        branch = get_branch()
    vars = {'branch': branch}
    with settings(warn_only=True, hide=('running', 'stderr')):
        result = git("show-ref --quiet --verify refs/heads/{branch}", **vars)
    if not result.succeeded:
        result = git("checkout -b {branch} {git_remote}/{branch}", **vars)
    else:
        result = git("checkout {branch}", **vars)

    if not result.succeeded:
        abort("Could not set branch to {0}".format(branch))


@task
def update_src():
    """
    Updates the source on the deployed git clone.

    This will call ``git pull <git_remote> <branch>`` on the deployed git
    clone to update the source. Both ``git_remote`` and ``branch`` are
    determined by environment variables. If ``branch`` is not set as an
    environment variable, your current local branch is used.

    If the server's branch and your local branch do not match, you will be
    prompted confirm whether or not to switch the branch. Answering "n" will
    abort the task. Answering "y" will call ``git fetch <git_remote>`` followed
    execution of the ``set_branch`` task before running the git pull.

    You may override the ``git_remote`` and/or ``branch`` environment variables
    using --set.
    """
    require('git_remote')

    branch = get_branch()

    with hide('running', 'stderr', 'stdout'):
        current_branch = git("rev-parse --abbrev-ref HEAD")

    if current_branch != branch:
        q = 'Switch branch from "{0}" to "{1}"?'
        q = q.format(current_branch, branch)
        if not promptbool(q):
            abort("Aborting...")

        git("fetch {git_remote}")
        execute(set_branch, host=env.host_string)

    git("pull {git_remote} {branch}", branch=branch)


@task
def clean_src(*args):
    """
    Calls ``git clean`` on the deployed git clone.

    Usage:

        clean_src
        clean_src:dirs

    This removes any files that do not belong in the repository. The command
    will first be run in "dry run" mode and will print the files that would be
    deleted, without actually deleting any files. You will then be prompted to
    continue, at which the command will be run using ``git clean -f`` to force
    removal of the files.

    By default, only files are removed. You may pass "dirs" as an argument to
    remove files as well as directories.

    This command runs as env.owner
    """
    require('src_dir', 'owner')

    cmd = 'git clean'
    if 'dirs' in args:
        cmd = 'git clean -d'

    with cd(env.src_dir):
        puts(red("DRY RUN:"))
        run(cmd + ' -n', user=env.owner)

    if promptbool("Continue with clean?"):
        run(cmd + ' -f', user=env.owner)


@task
def update_venv(pkg=None, upgrade=False):
    """
    Updates the virtualenv with the configured requirements.txt file.

    Usage:

        update_venv
        update_venv:pkg
    """
    require('venv', 'requirements')
    opts = []
    if upgrade:
        opts.append('--upgrade')

    if pkg:
        req = get_requirement(pkg)
        if not req:
            abort("Package not found: " + pkg)
        opts.append(req)
    else:
        opts.append('-r {0}'.format(env.requirements))

    opts = ' '.join(opts)
    run('{0}/bin/pip install {1}'.format(env.venv, opts), user=env.owner)


@task
def upgrade_venv(pkg=None):
    """
    Same as update_venv, but passes adds the --upgrade argument when calling
    pip install.
    """
    update_venv(pkg, upgrade=True)


@task
def list_venv():
    """
    Show packages installed in virtualenv
    """
    run("{0}/bin/pip list 2> /dev/null || {0}/bin/pip freeze".format())


@task
def reload():
    """
    Restart the running uwsgi process.

    Usage:

        reload

    This will simply touch the uwsgi touch-reload file to restart the process
    """
    require('touch_reload')
    run("touch {touch_reload}".format(**env))


@task
def migrate(app=None):
    """
    Runs django's migrate management command.

    Usage:

        migrate
        migrate:<app_name>

    By default, runs ``manage.py migrate`` on the server. Passing
    an ``app_name`` argument will only migrate that app.
    """
    cmd = 'migrate'
    if app:
        cmd = cmd + ' ' + app
    execute(manage, cmd=cmd, host=env.host_string)


@task
def update_bower(*args, **kwargs):
    """
    Updates bower packages

    Usage:

        update_bower
    """
    opts = []
    opts.extend(['--{0}'.format(a) for a in args])
    opts.extend(['--{0}={1}'.format(k, v) for k, v in kwargs.iteritems()])
    execute(manage, cmd='bower update ' + ''.join(opts), host=env.host_string)


@task
def collectstatic(*args, **kwargs):
    """
    Updates bower packages and runs the collectstatic management command.

    Usage:

        collectstatic
        collectstatic:nobower
    """
    opts = ['--noinput']
    opts.extend(['--{0}'.format(a) for a in args])
    opts.extend(['--{0}={1}'.format(k, v) for k, v in kwargs.iteritems()])
    execute(manage, cmd='collectstatic ' + ''.join(opts), host=env.host_string)


@task
def tag_deployment():
    """
    Tag the current deployment.

    This creates tags that point the latest deployed commits:

        deployment/<envname>/YYYY-MM-DD_HH-MM-SS
        deployment/<envname>/current

    ``envname`` corresponds with the current role name (pased to the fab
    command via -R). The ``current`` tag is always overwritten to point to the
    latest deployed commeit for that environemnt.
    """
    env.now = env.get('now', datetime.today())
    tags = [
        'deployment/{envname}/{now:%Y-%m-%d_%H-%M-%S}',
        'deployment/{envname}/current'
    ]

    for tag in tags:
        tag = tag.format(**env)
        local("git tag -f {tag} HEAD".format(tag=tag))
    local("git push -f --tags")

    # fetch and prune tags on server
    with hide('running', 'stdout'):
        git('fetch --prune --tags')


@task
def deploy(*skip):
    """
    Perform a full deployment to the server.

    Calls the following tasks in order:

        update_src
        migrate
        collectstatic
        reload
        tag_deployment

    Each task is executed in succession on each host. For example, if you call:

        fab -R staging,production deploy

    if you pass "no_<taskname>" as an argument, that task will be skipped, eg:

        fab -R production deploy:no_migrate

    Commands will be executed in the following fashion:

        for each role:
            for each host in role:
                update_src
                migrate
                collectstatic
                reload
                tag_deployment
    """
    subtasks = (
        'update_src',
        'migrate',
        'collectstatic',
        'reload',
        'tag_deployment'
    )
    for task in subtasks:
        if 'no_' + task in skip:
            continue
        execute(task, host=env.host_string)


@task
def show_env(*vars):
    """
    Show current environment variables for each host

    Usage:

        show_env
        show_env:<var1>[,<var2>...]

    Any number of arguments may be passed to show those environment variables.
    With no arguments, all variables are shown for each host.

    Environment variables can be different for each role, so be sure to pass -R
    to test how they will look with different roles.

    Environment variables may be overridden using --set. Values are formatted
    using ``value.format(**env)`` where ``env`` is the fabric environment.

    """
    items = list(env.items())
    if vars:
        items = list((v, env.get(v)) for v in vars)
    out = ''
    if len(items) > 1:
        out = '\n'
    puts(out + '\n'.join('{0} = {1}'.format(cyan(k), v) for k, v in items if k
                         in env))


# --| Misc. utility functions |------------------------------------------------

def run(*args, **kwargs):
    """
    Overrides run to accept ``user`` argument. If ``user`` is different from
    env.user, it will automatically use sudo to run the command.
    """
    if kwargs.get('user') and kwargs['user'] != env.user:
        return sudo(*args, **kwargs)
    kwargs.pop('user', None)
    return fab_run(*args, **kwargs)


def promptbool(msg, default="n"):
    answer = prompt(msg + " (y/n)", default=default, validate=r'^[yYnN]$')
    return bool(re.match(r'^[yY]$', answer))


def git(cmd, **vars):
    require('git_user', 'src_dir')
    vars = dict(env, **vars)
    if not vars.get('branch'):
        vars['branch'] = get_branch()
    with cd(env.src_dir):
        return run('git ' + cmd.format(**vars), user=env.git_user)


def get_local_branch():
    with hide('running', 'stdout', 'stderr'):
        # get head and make sure it's a valid branch
        branch = local("git rev-parse --abbrev-ref HEAD", capture=True)
        with settings(warn_only=True):
            cmd = ("git show-ref --quiet --verify \"refs/heads/{0}\""
                   .format(branch))
        if local(cmd).succeeded:
            return branch
    # not currently on a branch
    return None


def get_branch():
    return env.get('branch') or get_local_branch() or env.default_branch


def remote_python(func):
    """
    Allows the decorated function to be called with a list of positional
    arguments to be sent to sys.argv and execute the function on the server.
    """
    def inner(*params):
        from inspect import getsourcelines
        from textwrap import dedent
        lines = getsourcelines(func)[0]
        lines = lines[lines[0].strip().startswith('@') and 2 or 1:]
        src = dedent(''.join(lines)).strip()
        cmd = '{0} -c "{1}"'.format(env.python, src)
        if params:
            cmd = cmd + ' ' + ' '.join(params)
        with hide('running'):
            return run(cmd, shell=False)
    return inner


def get_requirement(pkg):
    reqfile = env.requirements

    @remote_python
    def get_req():
        import sys
        from pip.req import parse_requirements
        for req in parse_requirements(sys.argv[1]):
            if req.name == sys.argv[2]:
                out = req.url and req.url or str(req.req)
                if req.editable:
                    out = '-e ' + out
                print(out)
                break

    result = get_req(reqfile, pkg)
    if not result.succeeded:
        return None
    return result

# Respect ~/.ssh/config
env.use_ssh_config = True

# Fix for proxycommand in ssh config
paramiko.proxycommand = paramiko.ProxyCommand
