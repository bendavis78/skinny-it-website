from django.contrib import admin

from admin_csv import CSVMixin
from . import models


class ContactAdmin(CSVMixin, admin.ModelAdmin):
    list_display = ['__str__', 'company', 'title', 'phone', 'city', 'state',
                    'opt_in']

admin.site.register(models.Contact, ContactAdmin)
