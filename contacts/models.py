from __future__ import unicode_literals

from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from localflavor.us.models import (
    USStateField, USZipCodeField, PhoneNumberField)


@python_2_unicode_compatible
class Contact(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    company = models.CharField(max_length=100, blank=True)
    title = models.CharField(max_length=100, blank=True)
    email = models.EmailField()
    phone = PhoneNumberField(blank=True)
    address_1 = models.CharField(max_length=255, blank=True)
    address_2 = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=100, blank=True)
    state = USStateField(blank=True)
    zip = USZipCodeField(blank=True)
    opt_in = models.BooleanField(default=False)

    def __str__(self):
        return '{0.first_name} {0.last_name} <{0.email}>'.format(self)
