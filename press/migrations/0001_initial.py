# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PressRelease',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('release_date', models.DateField(default=datetime.datetime.now)),
                ('publish_date', models.DateField(null=True, blank=True, help_text='Leave empty to publish on release date')),
                ('headline', models.CharField(max_length=200)),
                ('slug', models.SlugField(unique=True)),
                ('location', models.CharField(max_length=100, blank=True)),
                ('body', models.TextField()),
                ('contact_info', models.TextField(blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
