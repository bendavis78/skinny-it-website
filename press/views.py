from django.shortcuts import render, get_object_or_404

from . import models


def index(request):
    releases = models.PressRelease.objects.order_by('-release_date')
    return render(request, 'press/index.html', {'press_releases': releases})


def release_detail(request, slug, **kwargs):
    release = get_object_or_404(models.PressRelease, slug=slug)
    return render(request, 'press/release.html', {'release': release})
