from django.conf import settings
from django.db import models

from siteprefs.toolbox import patch_locals, register_prefs, pref


DATE_FORMAT = getattr(settings, 'PRESS_DATE_FORMAT', settings.DATE_FORMAT)
DEFAULT_LOCATION = getattr(settings, 'PRESS_DEFAULT_LOCATION', '')
DEFAULT_CONTACT_INFO = getattr(
    settings, 'PRESS_DEFAULT_CONTACT_INFO', ' ')

patch_locals()

register_prefs(
    pref(DEFAULT_LOCATION, static=False,
         field=models.CharField(max_length=100)),
    pref(DEFAULT_CONTACT_INFO, static=False)
)
