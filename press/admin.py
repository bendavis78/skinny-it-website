from django.contrib import admin

from . import models
from . import settings


class PressReleaseAdmin(admin.ModelAdmin):
    list_display = ['headline', 'release_date', 'publish_date']
    fields = ['release_date', 'publish_date', 'headline', 'location',
              'body', 'contact_info']

    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, **kwargs)
        form.base_fields['location'].initial = settings.DEFAULT_LOCATION
        form.base_fields['contact_info'].initial = settings.DEFAULT_CONTACT_INFO
        return form

admin.site.register(models.PressRelease, PressReleaseAdmin)
