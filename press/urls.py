from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='press-index'),
    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>.+)/$',
        views.release_detail, name='press-release')
]
