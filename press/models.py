from __future__ import unicode_literals

from datetime import datetime

from django.core.urlresolvers import reverse
from django.db import models
from django.utils.dateformat import format as format_date
from django.utils.encoding import python_2_unicode_compatible
from django.utils.text import slugify


from . import settings


@python_2_unicode_compatible
class PressRelease(models.Model):
    release_date = models.DateField(default=datetime.now)
    publish_date = models.DateField(null=True, blank=True, help_text=(
        'Leave empty to publish on release date'))
    headline = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    location = models.CharField(blank=True, max_length=100)
    body = models.TextField()
    contact_info = models.TextField(blank=True)

    def save(self, *args, **kwargs):
        if not self.pk and not self.location:
            self.location = settings.DEFAULT_LOCATION

        if not self.pk and not self.contact_info:
            self.contact_info = settings.DEFAULT_CONTACT_INFO

        if not self.slug:
            self.slug = slugify(self.headline[:50].strip())

        if not self.publish_date:
            self.publish_date = self.release_date

        super().save()

    def as_markdown(self):
        article = ''
        release_date = format_date(self.release_date, settings.DATE_FORMAT)
        if self.release_date > self.publish_date:
            article += '**HOLD FOR RELEASE UNTIL ' + release_date + '**\n\n'
        else:
            article += '**FOR IMMEDIATE RELEASE**\n\n'
        article += '## ' + self.headline + '\n\n'
        if self.location:
            article += '*' + self.location.upper() + '* - '
        article += '*' + release_date + '*\n\n'
        article += self.body + '\n\n'
        if self.contact_info.strip():
            contact_info = self.contact_info.strip().replace('\r\n', '\n')
            article += contact_info.replace('\n', '  \n') + '\n\n'
        article += ' ###'  # leading space to avoid md header formatting
        return article

    @property
    def release_month(self):
        return self.release_date.strftime('%B %Y')

    def get_absolute_url(self):
        return reverse('press-release', kwargs={
            'year': self.release_date.year,
            'month': '{:02d}'.format(self.release_date.month),
            'day': '{:02d}'.format(self.release_date.day),
            'slug': self.slug
        })

    def __str__(self):
        return self.headline
