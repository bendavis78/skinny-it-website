from django.conf import settings

DOWNLOAD_EMAIL_FORM = getattr(settings, 'WHITEPAPERS_DOWNLOAD_EMAIL_FORM', None)
FROM_EMAIL = getattr(settings, 'WHITEPAPERS_FROM_EMAIL',
                     getattr(settings, 'DEFAULT_FROM_EMAIL', None))
