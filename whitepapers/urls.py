from django.conf.urls import url

from . import views


urlpatterns = [
    url('^$', views.index, name='whitepapers-index'),
    url('^(?P<slug>[^/]+)/$', views.summary, name='whitepapers-summary'),
    url('^(?P<slug>[^/]+)/sent/$', views.email_sent,
        name='whitepapers-email-sent')
]
