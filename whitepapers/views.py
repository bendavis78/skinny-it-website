from __future__ import unicode_literals

import os
import mimetypes

from django.utils.importlib import import_module
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404

from . import models
from . import settings


def index(request):
    whitepapers = models.Whitepaper.objects.order_by('-created_date')
    context = {'whitepapers': whitepapers}
    return render(request, 'whitepapers/index.html', context)


def summary(request, slug):
    whitepaper = get_object_or_404(models.Whitepaper, slug=slug)
    if not getattr(settings, 'DOWNLOAD_EMAIL_FORM'):
        return redirect(whitepaper.document.url)

    mod_name, form_name = settings.DOWNLOAD_EMAIL_FORM.rsplit('.', 1)
    mod = import_module(mod_name)
    EmailForm = getattr(mod, form_name)

    if request.method == 'POST':
        form = EmailForm(request.POST)
        if form.is_valid():

            contact = form.save()
            filename = os.path.basename(whitepaper.document.name)
            contents = whitepaper.document.read()
            type, _ = mimetypes.guess_type(filename)

            email = EmailMessage()
            email.from_email = settings.FROM_EMAIL
            email.subject = 'Your Skinny IT Whitepaper'
            email.body = 'The whitepaper you requested is attached.'
            email.attach(filename, contents, type)
            email.to = [contact.email]
            email.send()

            return redirect(reverse('whitepapers-email-sent',
                                    kwargs={'slug': slug}))
    else:
        form = EmailForm()

    context = {
        'whitepaper': whitepaper,
        'form': form
    }

    return render(request, 'whitepapers/summary.html', context)


def email_sent(request, slug):
    whitepaper = get_object_or_404(models.Whitepaper, slug=slug)
    context = {
        'whitepaper': whitepaper
    }
    return render(request, 'whitepapers/email_sent.html', context)
