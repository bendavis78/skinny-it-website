from django.contrib import admin

from . import models


class WhitepaperAdmin(admin.ModelAdmin):
    list_display = ['title', 'created_date']
    fields = ['title', 'summary', 'document']

admin.site.register(models.Whitepaper, WhitepaperAdmin)
