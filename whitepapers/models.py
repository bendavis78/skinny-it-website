from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.text import slugify
from django.utils import timezone


@python_2_unicode_compatible
class Whitepaper(models.Model):
    created_date = models.DateTimeField(default=timezone.now)
    title = models.CharField(max_length=250)
    slug = models.SlugField(unique=True)
    summary = models.TextField()
    document = models.FileField()

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('whitepapers-summary', kwargs={'slug': self.slug})

    def get_download_url(self):
        return reverse('whitepapers-download', kwargs={'slug': self.slug})

    @property
    def created_month(self):
        return self.created_date.strftime('%B %Y')

    def __str__(self):
        return self.title
