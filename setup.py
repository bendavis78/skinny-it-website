from setuptools import setup, find_packages

setup(
    name='skinnyit-website',
    version='0.1-dev',
    test_suite='skinnyit.tests',
    packages=find_packages(),
    install_requires=[],
    package_data={'skinnyit': []},
    include_package_data=True,
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.rst').read(),
)
