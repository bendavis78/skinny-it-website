"""
Django settings for skinnyit project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
# BASE_DIR is the project's VCS root.
import os
BASE_DIR = os.path.join(os.path.dirname(__file__), '..', '..')


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# To generate the secret key, run the following command:
# tr -cd '[:alnum:]' < /dev/urandom | head -c 32 > django-key
SECRET_KEY_FILE = os.path.join(BASE_DIR, '..', 'django-key')
SECRET_KEY = open(SECRET_KEY_FILE).read()

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = [
    'www.skinnyit.com'
]

ADMINS = (
    ('Ben Davis', 'ben@bendavisdigital.com'),
)

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'siteprefs',
    'skinnyit',
    'contacts',
    'whitepapers',
    'press',
    'markdown_deux',
    'widget_tweaks',
    'admin_csv'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages'
)

ROOT_URLCONF = 'skinnyit.urls'

WSGI_APPLICATION = 'skinnyit.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/Chicago'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

PRESS_DEFAULT_LOCATION = 'Dallas, Texas'

WHITEPAPERS_DOWNLOAD_EMAIL_FORM = 'contacts.forms.ContactForm'

DEFAULT_FROM_EMAIL = 'info@skinnyit.com'
CONTACT_NOTIFICATION_RECIPIENTS = ['test@example.com']
