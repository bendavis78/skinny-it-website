from skinnyit.settings.defaults import *  # noqa

DEBUG = False
DATABASES['default']['NAME'] = 'skinnyit_production'
STATIC_ROOT = os.path.join(BASE_DIR, '..', 'static')
MEDIA_ROOT = os.path.join(BASE_DIR, '..', 'media')
RAVEN_CONFIG = {
    'dsn': ('http://dc8375c298654cdda2e348da02632dee'
            ':ff2e1229c1f145ad9f4556706ab46ccd'
            '@sentry.bendavisdigital.com/2'),
}

INSTALLED_APPS = INSTALLED_APPS + (
    'raven.contrib.django.raven_compat',
)

CONTACT_NOTIFICATION_RECIPIENTS = ['kevinl@skinnyit.com']
