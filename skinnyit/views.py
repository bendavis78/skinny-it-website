from __future__ import unicode_literals

from posixpath import basename

from django import http
from django.shortcuts import render, redirect
from django.template import TemplateDoesNotExist

from whitepapers.models import Whitepaper

from . import utils
from . import forms


def get_context_data(request):
    # lazy man's middleware
    return {
        'has_whitepapers': Whitepaper.objects.all().count() > 0
    }


def page(request, name):
    name = name.strip('/') or 'index'
    if name.endswith('.html'):
        return redirect(basename(name).replace('.html', '') + '/')
    try:
        tpl = utils.get_page_template(name)
    except TemplateDoesNotExist:
        raise http.Http404()

    context = get_context_data(request)

    return render(request, tpl.name, context)


def contact(request):
    opt_in_form = forms.OptInForm()
    contact_form = forms.EmailContactForm()
    if request.method == 'POST':
        opt_in_form = forms.OptInForm(request.POST)
        contact_form = forms.EmailContactForm(request.POST)
        type = request.POST.get('type')
        if type == 'contact':
            form = contact_form
        else:
            form = opt_in_form
        if form.is_valid():
            form.save()
            return redirect('thank-you/')

    context = {
        'contact_form': contact_form,
        'opt_in_form': opt_in_form
    }

    return render(request, 'skinnyit/contact.html', context)
