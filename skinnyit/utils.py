from django.template import loader, TemplateDoesNotExist


def get_page_template(path):
    path = path.strip('/') or 'index'
    try:
        tpl = 'skinnyit/pages/{}/index.html'.format(path)
        return loader.get_template(tpl)
    except TemplateDoesNotExist:
        tpl = 'skinnyit/pages/{}.html'.format(path)
        return loader.get_template(tpl)
