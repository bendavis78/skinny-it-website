from django import forms
from django.conf import settings
from django.core.mail import EmailMessage
from django.template import loader, Context

from contacts.forms import ContactForm as OptInForm


class EmailContactForm(OptInForm):
    comments = forms.CharField(widget=forms.Textarea)

    def save(self, *args, **kwargs):
        contact = super().save()

        # send notification email
        email = EmailMessage()
        email.subject = (
            'Skinny IT - Contact Form Submission from {}'
            .format(str(contact)))
        tpl = loader.get_template('skinnyit/contact_notification.txt')
        email.body = tpl.render(Context(self.cleaned_data))
        email.from_email = settings.DEFAULT_FROM_EMAIL
        email.to = settings.CONTACT_NOTIFICATION_RECIPIENTS
        email.send()

        return contact
