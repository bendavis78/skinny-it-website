$(document).ready(function() {
	
	var $win = $(window),
		$topnav = $('.top-nav').eq(0),
		$topnavmenu = $('.top-nav-menu').eq(0);
	
	// open and close mobile nav
	$('#menu-toggle').click(function(){
		$topnav.toggleClass('menu-open');
		$topnavmenu.slideToggle();
	});
	
	// hide mobile secondary nav gracefully
	$('html').addClass('hide-nav');
	
	// open and close mobile secondary nav
	$topnavmenu.find('.drop > a').click(function(e){
		if ($win.width() < 1025) {
			var $a = $(this);
			$a.closest('.drop').toggleClass('open');
			$a.next('ul').slideToggle();
			$a.blur();
			e.preventDefault();
		}		
	});
	
});