from django.conf.urls import include, url
from django.contrib import admin
from siteprefs.toolbox import autodiscover_siteprefs
from skinnyit import views

import press.urls
import whitepapers.urls


autodiscover_siteprefs()

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^press/', include(press.urls)),
    url(r'^white-papers/', include(whitepapers.urls)),
    url(r'^contact/$', views.contact, name='skinnyit-contact'),
    url(r'^(?P<name>.*)/$', views.page, name='skinnyit-page'),
    url(r'^$', views.page, name='skinnyit-page', kwargs={'name': 'index'})
]
