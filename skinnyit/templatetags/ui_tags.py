import re
from copy import copy

from django.template import TemplateDoesNotExist, TemplateSyntaxError, Library
from django.core.cache import cache

from skinnyit import utils

register = Library()
title_re = re.compile(r'<title>(Skinny IT : )?(.*?)( Overview)?( : Skinny IT)?'
                      '</title>')


@register.inclusion_tag("skinnyit/breadcrumbs.html", takes_context=True)
def breadcrumbs(context):
    if context.get('skip_breadcrumbs'):
        return context
    request = context['request']
    path_parts = request.path.strip('/').split('/')
    breadcrumbs = [('/', 'Home')]
    pfx = 'skinnyit-page-title-'

    for i, part in enumerate(path_parts):
        path = '/' + '/'.join(path_parts[:i+1])
        key = pfx + ('-'.join(path_parts[:i+1]) or 'home')
        title = cache.get(key)
        if not title:
            try:
                tpl = utils.get_page_template(path)
                with context.push({'skip_breadcrumbs': True}):
                    html = tpl.render(context)
            except (TemplateDoesNotExist, TemplateSyntaxError):
                return {}
            match = title_re.search(html)
            title = match and match.groups()[1] or '(no title)'
            cache.set(key, title)
        breadcrumbs.append((path, title))

    return {
        'breadcrumbs': breadcrumbs
    }


@register.inclusion_tag("skinnyit/form_field.html", takes_context=True)
def form_field(context, field, **kwargs):
    kwargs['field'] = field
    ctx = copy(context)
    ctx.push(kwargs)
    return ctx
